const User = require('../../models/user');
var createError = require('http-errors');
const { create } = require('../../models/user');
const tokens = require('../../jwt/token');

exports.list = (req, res, next)=>{
    User.getAll()
    .then(users=>{
        res.status(200).send({users: users});
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    })
}

exports.get = (req, res, next)=>{
    var user = res.user;
    return res.status(200).send({user: user});
}

exports.create = (req, res, next)=>{
    User.add(req.body.user).then(user=>{
        user.enviar_email_bienvenida();
        res.status(200).send({user: user});
    }).catch(err=>{
        next(createError(err.statusCode, err.message));
    });
}

exports.reset_password = (req, res, next)=>{
    var user = res.user;
    user.enviar_email_reset_password();
    res.status(200).send({message: 'Se ah enviado el correo'});
}

exports.login = (req, res, next) =>{
    if(!req.body.email || !req.body.password) return next(createError(400, 'Faltan campos obligatorios'));
    User.findByEmailOrUsernameWithScopes(req.body.email)
        .then(user=>{
            if(!user) throw createError(404, 'No se encontro el usuario');
            if(!user.validPassword(req.body.password)) throw createError(401, 'Las credenciales enviadas no son correctas');
            // juntar los scopes
            let scopes = [];
            user.scopes.forEach(element => {
                scopes.push(element.name);
            });
            user.roles.forEach(rol=>{
                rol.scopes.forEach(element=>{
                  scopes.push(element.name);
                })
            });
             /* Almacenar usuario ... */
            var userToken = {
                id : user.id,
                username : user.username,
                email : user.email,
                scopes : scopes
            }
            return tokens.generateToken(userToken);
            res.send(scopes);
        })
        .then(token=>{
            res.status(200).json({
                login: true,
                token: token
              });
        })
        .catch(err=>{next(createError(err.statusCode, err.message))});   
};

/**
 * ! Revisar si todo se puede modificar o no
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.update = (req, res, next)=>{
    var user = res.user;
    if(req.body.user.passowrd) next(createError(400, 'El password no puede modificarse desde aqui'));
    Object.assign(user, req.body.user); // Modificar propiedades
    user.save().then(user=>{
        res.status(200).send({user: user});
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    });
}

exports.delete = (req, res, next)=>{
    var user = res.user;
    user.remove().then(data=>{
        res.status(200).send({message: 'Usuario eliminado correctamente', user: data});
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    });
}

exports.getElementMiddleware = (req, res, next)=>{
    User.findById(req.params.id).then(user=>{
      if(!user) return res.status(404).send({message: `No se encontró el usuario con id: ${req.params.id}`});
      res.user = user;
      next();
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    })
}