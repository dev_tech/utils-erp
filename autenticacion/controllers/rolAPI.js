const Rol = require('../../models/rol');
var createError = require('http-errors');

exports.list = (req, res, next)=>{
    Rol.getAll()
    .then(roles=>{
        res.status(200).send({roles: roles});
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    })
}

exports.get = (req, res, next)=>{
    var rol = res.rol;
    return res.status(200).send({rol: rol});
}

exports.create = (req, res, next)=>{
    Rol.add(req.body.rol).then(rol=>{
        res.status(200).send({rol: rol});
    }).catch(err=>{
        next(createError(err.statusCode, err.message));
    });
}

exports.update = (req, res, next)=>{
    var rol = res.rol;
    Object.assign(rol, req.body.rol); // Modificar propiedades
    rol.save().then(rol=>{
        res.status(200).send({rol: rol});
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    });
}

exports.delete = (req, res, next)=>{
    var rol = res.rol;
    rol.remove().then(data=>{
        res.status(200).send({message: 'Rol eliminado correctamente', rol: data});
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    });
}

exports.getElementMiddleware = (req, res, next)=>{
    Rol.findById(req.params.id).then(rol=>{
      if(!rol) return res.status(404).send({message: `No se encontró el rol con id: ${req.params.id}`});
      res.rol = rol;
      next();
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    })
}




