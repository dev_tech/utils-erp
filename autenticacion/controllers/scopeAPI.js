const Scope = require('../../models/scope');
var createError = require('http-errors');

exports.list = (req, res, next)=>{
    Scope.getAll()
    .then(scopes=>{
        res.status(200).send({scopes: scopes});
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    })
}

exports.get = (req, res, next)=>{
    var scope = res.scope;
    return res.status(200).send({scope: scope});
}

exports.create = (req, res, next)=>{
    Scope.add(req.body.scope).then(scope=>{
        res.status(200).send({scope: scope});
    }).catch(err=>{
        next(createError(err.statusCode, err.message));
    });
}

exports.update = (req, res, next)=>{
    var scope = res.scope;
    Object.assign(scope, req.body.scope); // Modificar propiedades
    scope.save().then(scope=>{
        res.status(200).send({scope: scope});
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    });
}

exports.delete = (req, res, next)=>{
    var scope = res.scope;
    scope.remove().then(data=>{
        res.status(200).send({message: 'Scope eliminado correctamente', scope: data});
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    });
}

exports.getElementMiddleware = (req, res, next)=>{
    Scope.findById(req.params.id).then(scope=>{
      if(!scope) return res.status(404).send({message: `No se encontró el scope con id: ${req.params.id}`});
      res.scope = scope;
      next();
    })
    .catch(err=>{
        next(createError(err.statusCode, err.message));
    })
}




