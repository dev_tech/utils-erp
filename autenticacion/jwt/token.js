const jwt = require('jsonwebtoken');
const fs = require('fs');
const PUBLIC_KEY = fs.readFileSync("./jwt/public.key", "utf8");
const PRIVATE_KEY = fs.readFileSync("./jwt/private.key", "utf8");

var i = 'Tech Web';
var s = 'dev@techweb.press';
var a = 'https://techwebgt.com';

exports.validateToken = (req, res, next) =>{
    const verifyOptions = {
        issuer: i,
        subject: s,
        audience: a,
        expiresIn: '6h',
        algorithm: ['RS256']
      };
    jwt.verify(req.headers['access-token'], PUBLIC_KEY, verifyOptions, function(err, decoded){
        if(err) res.status(401).send({message : err.message});
        else {
            res.token = decoded;
            // En un futuro validar expiracion
            next();
        }
    })
};

exports.generateToken = (user)=>{
    var signOptions = {
        issuer: i,
        subject: s,
        audience: a,
        algorithm: "RS256" // RSASSA [ "RS256", "RS384", "RS512" ]
      };
    var payload = {
      auth: "true",
      exp: Math.floor(Date.now() / 1000) + 6 * 60 * 60, //6 horas
      user: user
    }
    return jwt.sign(payload, PRIVATE_KEY, signOptions);
}