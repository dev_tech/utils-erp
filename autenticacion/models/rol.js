const mongoose = require('mongoose');
var rolSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    scopes: [{type: mongoose.SchemaTypes.ObjectId, ref: 'Scope'}]
  });

  rolSchema.statics.getAll = function(){
    return this.find().populate('scopes', 'name');
  };

  rolSchema.statics.add = function(aElement){
    if(!aElement.name) throw createError(400, 'Faltan campos obligatorios');
    const element = new this({...aElement});
    return element.save();
  };

  module.exports = mongoose.model('Rol', rolSchema);