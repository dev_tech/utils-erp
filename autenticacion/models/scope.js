const mongoose = require('mongoose');
var createError = require('http-errors');

var scopeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
  })

  scopeSchema.statics.getAll = function(){
    return this.find();
  }

  scopeSchema.statics.add = function(aElement){
    if(!aElement.name) throw createError(400, 'Faltan campos obligatorios');
    const element = new this({...aElement});
    return element.save();
  }

  module.exports = mongoose.model('Scope', scopeSchema)