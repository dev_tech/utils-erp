const mongoose = require('mongoose');
var createError = require('http-errors');
const mailer = require('../mailer/mailer');

const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 12;



const Token = require('../models/token');

var uniqueValidator = require('mongoose-unique-validator');
const validate_email = email =>{
  const re = /^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
  return re.test(email);
}

const userSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    lastName: {
      type: String,
      required: true
    },
    username: {
        type: String, 
        required: true,
        unique: true,
        trim: true
    },
    email: {
        type: String,
        required: true,
        unique: true,
        trim: true,
        lowercase: true,
        validate: [validate_email, 'Ingrese un email valido'],
        match: [/^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/]
    },
    tel: [],
    password: {
        type: String,
        required: true
    },
    passwordResetToken: String,
    passowrdResetTokenExpires: Date,
    verificado:{
      type: Boolean,
      default: false
    },
    roles: [{type: mongoose.SchemaTypes.ObjectId, ref: 'Rol'}],
    scopes: [{type: mongoose.SchemaTypes.ObjectId, ref: 'Scope'}]
  });

  userSchema.statics.getAll = function(){
    return this.find({});
  }

  userSchema.statics.add = function(aUser){
    if(!aUser.username||!aUser.email||!aUser.password||!aUser.name||!aUser.lastName) throw createError(400, 'Faltan campos obligatorios');
    const user = new this({...aUser});
    return user.save();
  }

  userSchema.statics.findByEmailOrUsername = function(valorBuscado){
    return this.findOne({$or: [{username: valorBuscado}, {email: valorBuscado}]});
  }

  userSchema.statics.findByEmailOrUsernameWithScopes = function(valorBuscado){
    return this.findOne({$or: [{username: valorBuscado}, {email: valorBuscado}]})
      .populate({'path' : 'scopes', 'select' : 'name'}).populate({'path' : 'roles', populate: {path: 'scopes', 'select' : 'name'}});
  }

  userSchema.plugin(uniqueValidator, {message: 'El {PATH} Ya existe un con otro usuario'});

  userSchema.pre('save', function(next){
    if(this.isModified('password')){
      this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
  });

  userSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(password, this.password);
  };

  userSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({userId: this._id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save()
      .then(data=>{
        const mailOption = {
          from: 'dev2@techweb.press',
          to: email_destination,
          subject: 'Verificacion de cuenta',
          text: `Hola, \n\n Por favor, para verificar su cuenta haga click en el siguiente enlace \n http://localhost:3000/token/confirmation/${token.token}\n`
        }
        mailer.sendMail(mailOption, (err)=>{
            if(err) console.log(err.message);
            console.log(`A verification email has been sent to ${email_destination}.`);
        })
      })
      .catch(err=>{
        console.log(err);
      });
  }

  userSchema.methods.enviar_email_reset_password = function(cb) {
    const token = new Token({userId: this._id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save()
      .then(data=>{
        const mailOption = {
          from: 'dev2@techweb.press',
          to: email_destination,
          subject: 'Reset Password',
          text: `Hola, \n\n Por favor, para reestablecer su contrase;a haga click en el siguiente enlace \n http://localhost:3000/token/reset-password/${token.token}\n este enlace tiene una validez de 24 horas.`
        }
        mailer.sendMail(mailOption, (err)=>{
            if(err) console.log(err.message);
            console.log(`A reset password email has been sent to ${email_destination}.`);
        })
      })
      .catch(err=>{
        console.log(err);
      });
  }

  module.exports = mongoose.model('User', userSchema)