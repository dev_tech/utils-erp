var express = require('express');
var router = express.Router();
var authController = require('../../controllers/api/authAPI');

router.get('/users', authController.list);
router.get('/users/:id', authController.getElementMiddleware, authController.get);
router.post('/sign-up', authController.create);
router.post('/reset-password/:id', authController.getElementMiddleware, authController.reset_password);
router.post('/login', authController.login);


router.patch('/users/:id', authController.getElementMiddleware, authController.update);
router.delete('/users/:id', authController.getElementMiddleware, authController.delete);

module.exports = router;