/**
 * Funcion para generar los siguientes pagos, esta se ejecuta cada día por todos los pagos programados que estén activos
 * @param {PagoProgramado} pago_viejo 
 */
function generar_siguiente_pago(pago_viejo) {
    if (pago.fecha_fin && pago.fecha_fin >= fecha_actual) return; // Si ya se vencio no lo genera
    if (pago.veces && pago.veces == 0) return; // No lo genera si ya se le agotaron las veces
    if (pago.veces) pago.veces--; // Disminuir las veces
    var fecha_siguiente_pago = null;
    switch (pago_viejo.tipo_periodicidad) {
        case "mensual":
            switch (pago_viejo.tipo_dia_generacion) {
                case "dia_especifico":
                    fecha_siguiente_pago = addNMonth(pago_viejo.fecha, pago_viejo.intervalo);
                    break;
                case "dia_relativo":
                    // Obtener el dia de la semana y el numero de semana, 
                    // ej para un valor de dia_generacion = 14 que equivaldria a un 3er domingo del mes el calculo seria:
                    // n = 14 / 7 = 2, que equivale a la tercera semana
                    // d = 14 - ( 7 * 2 ) = 0, lo que equivale a domingo
                    var aux_date = addNMonth(pago_viejo.fecha, pago_viejo.intervalo);
                    var n = ~~(pago_viejo.dia_generacion / 7); // Numero de semana del mes
                    var d = pago_viejo.dia_generacion - (7 * n); // Dia de la semana
                    var m = aux_date.getMonth();
                    var y = aux_date.getFullYear();
                    fecha_siguiente_pago = getMonthlyWeekday(n, d, m, y);
                    break;
            }
            break;
        case "semanal":
            fecha_siguiente_pago = addNWeek(pago_viejo.fecha, pago_viejo.intervalo);
            break;
        case "diario":
            fecha_siguiente_pago = addNDay(pago_viejo.fecha, pago_viejo.intervalo);
            break;
        case "anual":
            fecha_siguiente_pago = addNYear(pago_viejo.fecha, pago_viejo.intervalo);
            break;
    }
    var newPago = new PagoProgramado(pago_viejo);
    newPago.fecha = fecha_siguiente_pago;
    newPago.save();
}

// Funcion que añade n meses a la fecha dada
function addNMonth(fecha, n) {
    var m = fecha.getMonth() + n;
    var y = fecha.getFullYear();
    if (m > 11) {
        m = m - 12;
        y = y + 1;
    }
    return new Date(y, m, fecha.getDate());
}

// Funcion que añade n semanas a la fecha dada
function addNWeek(fecha, n) {
    // (días * 24 horas * 60 minutos * 60 segundos * 1000 milésimas de segundo) 
    milisegundos_a_sumar = (7 * n) * 24 * 60 * 60 * 1000; // se le suman (siete * n) días
    fechaConNSemanasMas = fecha.getTime() + (milisegundos_a_sumar);
    return new Date();
}

// Funcion que añade n dias a la fecha dada
function addNDay(fecha, n) {
    // (días * 24 horas * 60 minutos * 60 segundos * 1000 milésimas de segundo) 
    milisegundos_a_sumar = n * 24 * 60 * 60 * 1000; // se le suman n días
    fechaConNDiasMas = fecha.getTime() + (milisegundos_a_sumar);
    return new Date();
}

// Funcion que añade n dias a la fecha dada
function addNYear(fecha, n) {
    // (días * 24 horas * 60 minutos * 60 segundos * 1000 milésimas de segundo)
    return fecha.setFullYear(fecha.getFullYear() + n);
}


/* JavaScript getMonthlyWeekday Function:
 *
 * Description:
 * Gets Nth weekday for given month/year. For example, it can give you the date of the first monday in January, 2017 or it could give you the third Friday of June, 1999. Can get up to the fifth weekday of any given month, but will return FALSE if there is no fifth day in the given month/year.
 *
 *
 * Parameters:
 *    n = 0-4 for first, second, third, fourth or fifth weekday of the month
 *    d = numero de dia de la semana 0-6 Sunday-Saturday 
 *    m = Numero del mes 0-11 Enero-Diciembre
 *    y = Four digit representation of the year like 2017
 *
 * Return Values:
 * returns 1-31 for the date of the queried month/year that the nth weekday falls on.
 * returns false if there isn't an nth weekday in the queried month/year
*/
function getMonthlyWeekday(n, d, m, y) {
    var targetDay, curDay = 0,
        i = 1,
        seekDay;
    seekDay = d;
    while (curDay < n && i < 31) {
        // Crea un ciclo que recorre todos los dias del mes y cada vez que pasa por el dia de la semana buscado suma el contador curDay
        targetDay = new Date(y, m, i++);
        if (targetDay.getDay() == seekDay) curDay++;
    }
    if (curDay == n) { // Se hace esta validacion por si no se encuentra el numero de fecha deseado
        targetDay = targetDay.getDate();
        return targetDay;
    } else {
        return new Date(y, m + 1, 0); // Devuelve el ultimo día del mes solicitado
    }
} //end getMonthlyWeekday JS function