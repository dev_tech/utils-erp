import { defaultAPIController } from "./defaultAPI.controller";

import createError from "http-errors";
import { JWT } from "../jwt/token";
import globalMongoConnection from "../helpers/db";
import { IBackendScope } from "../models/backendScope.model";
import { IFrontendScope } from "../models/frontendScope.model";
import { IUser } from "../models/user.model";
import { IRol } from "../models/rol.model";

const BackendScope = require("../models/backendScope.model");
const FrontendScope = require("../models/frontendScope.model");
const RefreshToken = require("../models/refresh-token.model");
const User = require("../models/user.model");
export class AuthController extends defaultAPIController {
  constructor() {
    super();
    this.elementModel = User;
    this.singleID = "user";
    this.pluralID = "users";
    this.modelName = "User";
    this.forceDB = "metabase";
  }

  refreshPassword = (req, res, next) => {
    // TODO un usuario puede ver la info de su propio usuario
    const model = globalMongoConnection.connection.useDb("metabase").model(this.modelName);
    if (!req.params.email) return next(createError(400, "Falta el email"));
    model
      .findByEmailOrUsername(req.params.email)
      .then((user) => {
        user.enviar_email_reset_password();
        res.status(200).send({
          message: "Se envio el email para el reestablecimiento de la contraseña",
        });
      })
      .catch((err) => {
        next(createError(404, "El email no pertenece a ningun usuario del sistema."));
      });
  };

  create = (req, res, next) => {
    const model = globalMongoConnection.connection.useDb("metabase").model(this.modelName);
    const modelScope = globalMongoConnection.connection.useDb("metabase").model("BackendScope");
    modelScope
      .find({ default: true })
      .then((scopes) => {
        const newUser = req.body;
        newUser.org = req.user.org;
        newUser.scopes = scopes.map((e) => e._id);
        return model.add(newUser);
      })
      .then((user) => {
        user.enviar_email_bienvenida();
        res.status(200).send(user);
      })
      .catch((err) => {
        next(createError(err.statusCode, err.message));
      });
  };

  getUserToGenToken(user: IUser) {
    // juntar los scopes
    //  console.log(user.roles[0].scopes);
    const backendScopes = [];
    const frontendScopes = [];
    user.scopes.forEach((element: IBackendScope) => {
      backendScopes.push(`[${element.httpVerb}]${element.url}`);
    });
    user.roles.forEach((rol: IRol) => {
      rol.scopes.forEach((element: IFrontendScope) => {
        // console.log(element);
        frontendScopes.push(element.id);
        element.backendScopes.forEach((scope: IBackendScope) => {
          backendScopes.push(`[${scope.httpVerb}]${scope.url}`);
        });
      });
    });
    // Eliminar duplicados
    const cleanBackendScopes = backendScopes.filter((el, index) => backendScopes.indexOf(el) === index);
    const userToken = {
      id: user.id,
      username: user.username,
      scopes: cleanBackendScopes,
      frontendScopes,
      org: user.org,
    };
    return userToken;
  }

  authenticate = (req, res, next) => {
    if (!req.body.username || !req.body.password) return next(createError(400, "Faltan campos obligatorios"));
    const ipAddress = req.ip;
    const model = globalMongoConnection.connection.useDb("metabase").model(this.modelName);
    model
      .findByEmailOrUsernameWithScopes(req.body.username) // Buscar al usuarios
      .then(async (user: IUser) => {
        // Crear token
        if (!user) throw createError(404, "No se encontro el usuario");
        if (!user.validPassword(req.body.password)) throw createError(401, "Las credenciales enviadas no son correctas");
        if (!user.verificado) throw createError(401, "La cuenta aun no ha sido verficada, por favor revise su correo electronico");
        if (!user.activo) throw createError(401, "La cuenta no se encuentra activa, contacte a su administrador");

        /* Almacenar usuario ... */
        const userToken = this.getUserToGenToken(user);
        // console.log(userToken);
        const jwtToken = JWT.generateToken(userToken);
        return {
          ...this.basicDetails(user),
          jwtToken,
          frontendScopes: userToken.frontendScopes,
        };
      })
      .then(({...user }) => {
        // Devolverlo
        res.status(200).json(user);
      })
      .catch((err) => {
        next(createError(err.statusCode, err.message));
      });
  };

  toggleActivation = (req, res, next) => {
    const user = res.element;
    user.activo = !user.activo;
    user
      .save()
      .then((user) => {
        res.status(200).send({ message: "Se actualizo el usuario correctamente" });
      })
      .catch((err) => {
        next(createError(err.statusCode, err.message));
      });
  };

  forceVerification = (req, res, next) => {
    const user = res.element;
    user.verificado = true;
    user
      .save()
      .then((user) => {
        res.status(200).send({ message: "Se actualizo el usuario correctamente" });
      })
      .catch((err) => {
        next(createError(err.statusCode, err.message));
      });
  };

  basicDetails(user) {
    const { id, username, name, lastName, roles, org } = user;
    return { id, username, name, lastName, roles, org };
  }
}
