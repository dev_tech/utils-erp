import createError from 'http-errors';
import mongoose, {Schema, Document, Model, DocumentQuery} from 'mongoose';
import { IBackendScope } from './backendScope.model';
import { IDefaultModel } from './default.model';


export interface IFrontendScope extends Document{
  _id: string;
  id: string;
  group: string;
  description: string;
  protected: boolean;
  backendScopes: IBackendScope[];
}

export interface IFrontendScopeModel extends IDefaultModel{
  getAll(query?: any): DocumentQuery<IFrontendScope[], IFrontendScope>
}

const schema = new mongoose.Schema({
  _id: {
    type: String,
    required: true,
    trim: true,
  },
    group:{
      type: String,
      required: true
    },
    description: {
      type: String,
      required: true
    },
    protected: {
      type: Boolean,
      required: true,
      default: false
    },
    backendScopes: [{type: String, ref: 'BackendScope'}]
  }, { _id: false, timestamps: true })

  schema.statics.getAll = function(query = {}){
    return this.find(query);
  }

  schema.statics.add = function(aElement){
    if(!aElement._id) throw createError(400, 'Faltan campos obligatorios');
    const element = new this({...aElement});
    return element.save();
  }

  schema.set('toJSON', {
    virtuals: true,
    versionKey: false,
    transform (doc, ret) {   delete ret._id  }
  });

  export default mongoose.model<IFrontendScope, IFrontendScopeModel>('FrontendScope', schema);