import jwt, { SignOptions } from 'jsonwebtoken';
import fs from 'fs';
import crypto from "crypto";
import globalMongoConnection from '../helpers/db';

const PUBLIC_KEY = fs.readFileSync("./src/jwt/public.key", "utf8");
const PRIVATE_KEY = fs.readFileSync("./src/jwt/private.key", "utf8");

const RefreshToken = require("../models/refresh-token.model");
const User = require("../models/user.model");

export class JWT{
  static s = 'dev@techweb.press';
  static i = 'Tech Web';
  static a = 'https://techwebgt.com';

  static validateToken = (req, res, next) =>{
      const verifyOptions = {
          issuer: JWT.i,
          subject: JWT.s,
          audience: JWT.a,
          expiresIn: '6h',
          algorithm: ['RS256']
        };
        const jwttoken = req.headers.authorization;
        if(!jwttoken)return res.status(401).send({message :"No tiene el permiso para acceder a este recurso"});
        const tokenArray = jwttoken.split(" ");
        jwt.verify(tokenArray[1], PUBLIC_KEY, verifyOptions, async (err, decoded:any)=>{
        if(err) res.status(401).send({message : err.message});
          else {
            const scopeRequired = `[${req.method}]${req.baseUrl}${Object.keys(req.params).length > 0?req.route.path:(req._parsedUrl.pathname==='/'?'':req._parsedUrl.pathname)}`;
            const findedPermision = decoded.user.scopes.indexOf(scopeRequired);
            const modelUser =  globalMongoConnection.connection.useDb("metabase").model('User');
            const modelRefreshToken =  globalMongoConnection.connection.useDb("metabase").model('RefreshToken');
            if(findedPermision>=0){
            // if(true){
               console.log('autorizado: '+scopeRequired);
               req.user = decoded.user;
               const refreshTokens = await modelRefreshToken.find({ user: decoded.user.id });
               req.user.ownsToken = token => {const ref = !!refreshTokens.find(x => x.token === token);return ref;}
               next();
              }else{
                console.log('no autorizado: '+scopeRequired);
                res.status(401).send({message :"No tiene el permiso para acceder a este recurso"});
              }
          }
      })
  };

  static generateToken = (user)=>{
      const signOptions: SignOptions = {
          issuer: JWT.i,
          subject: JWT.s,
          audience: JWT.a,
          algorithm: "RS256" // RSASSA [ "RS256", "RS384", "RS512" ]
        };
      const payload = {
        auth: "true",
        exp: Math.floor(Date.now() / 1000) + 6 * 60 * 60, // 6 horas
        user
      }
      return jwt.sign(payload, PRIVATE_KEY, signOptions);
  }

  static generateRefreshToken = (user, ipAddress) => {
    // RefreshToken.find();
    const modelRefreshToken =  globalMongoConnection.connection.useDb("metabase").model('RefreshToken');
    // create a refresh token that expires in 7 days
    return new modelRefreshToken({
        user: user.id,
        token: JWT.randomTokenString(),
        expires: new Date(Date.now() + 7*24*60*60*1000),
        createdByIp: ipAddress
    });
  }

  static randomTokenString() {
    return crypto.randomBytes(40).toString('hex');
  }
}

