import { defaultAPIController } from "./defaultAPI.controller";

const User = require('../models/user.model');
const Token = require('../models/token.model');
import Logging from '../controllers/logging.controller';

import createError from "http-errors";
import {JWT} from"../jwt/token";
import { IDefaultModel } from "../models/default.model";
import globalMongoConnection from '../helpers/db';

export class TokenController{
    elementModel: IDefaultModel;
    singleID: string;
    pluralID: string;
    modelName: string;
    constructor(){
        this.elementModel = Token;
        this.singleID = "token";
        this.pluralID = "tokens";
        this.modelName = 'Token';
    }



confirmationToken = async (req, res, next)=>{
    const model =  globalMongoConnection.connection.useDb("metabase").model(this.modelName);
    const modelUser =  globalMongoConnection.connection.useDb("metabase").model('User');
    try {
        const token = await model.findOne({"token":req.params.id});
        if(token){
            const {err,result} = await modelUser.updateOne(
                { "_id":token.userId},{ verificado: true } );
            try {
                await model.findByIdAndDelete(token._id);
            } catch (error) {
                Logging.error(error);
                return res.redirect(`${process.env.FRONTURL}/login?message=${error.message}&type=danger`);
            }
            // res.send({status:true, data:token, message:"Email confirmado."});
            return res.redirect(`${process.env.FRONTURL}/login?message=Su cuenta ah sido confirmada, puede ingresar sesion&type=success`);
        }else{
            return res.redirect(`${process.env.FRONTURL}/login?message=No se pudo confirmar la cuenta, el token se vencio&type=danger`);
        }
    } catch (err) {
        Logging.error(err);
        next(createError(err.statusCode, err.message));
    }
}

refreshPassword = async (req, res, next)=>{
    const model =  globalMongoConnection.connection.useDb("metabase").model(this.modelName);
    const modelUser =  globalMongoConnection.connection.useDb("metabase").model('User');
    if(!req.body.password) return next(createError(400, 'Falta la contraseña a cambiar'));
    try {
        const token = await model.findOne({"token":req.params.id});
        if(token){
            modelUser.findById(token.userId).then(user=>{
                user.password = req.body.password;
                return user.save();
            })
            .then(user=>{
                return model.findByIdAndDelete(token._id);
            })
            .then(resp=>{
                res.status(200).send({message: 'La contraseña se actualizo correctamente'});
            })
            .catch(err=>{
                next(createError(err.statusCode, err.message));
            })
        }else{
            next(createError(400, 'Token vencido'));
        }
    } catch (err) {
        Logging.error(err);
        next(createError(err.statusCode, err.message));
    }
}
}