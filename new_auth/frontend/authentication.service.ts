﻿import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { BehaviorSubject, Observable } from "rxjs";
import { map } from "rxjs/operators";

import { environment } from "@environments/environment";
import { User } from "@app/_models";
import { Org } from "@app/_models/org";
import { OrgsService } from "./orgs.service";
import { NGXLogger } from "ngx-logger";
import { LoggingService } from "./logging.service";

@Injectable({ providedIn: "root" })
export class AuthenticationService {
  private userSubject: BehaviorSubject<User>;
  public user: Observable<User>;
  private orgSubject: BehaviorSubject<Org>;
  public org: Observable<Org>;

  constructor(
    private router: Router,
    private http: HttpClient,
  ) {
    this.userSubject = new BehaviorSubject<User>(null);
    this.user = this.userSubject.asObservable();
  }
  public get userValue(): User {
    return this.userSubject.value;
  }

  login(username: string, password: string) {
    return this.http
      .post<any>(
        `${environment.apiUrl}/api/auth/authenticate`,
        { username, password },
        { withCredentials: true }
      )
      .pipe(
        map((user) => {
          this.userSubject.next(new User(user));
          return user;
        })
      );
  }

  sign_up(user: User) {
    return this.http.post<any>(`${environment.apiUrl}/api/auth/sign-up`, user, {
      withCredentials: true,
    });
  }

  logout() {
    this.userSubject.next(null);
    this.router.navigate(["/login"]);
  }

  

  refreshPasswordGenerate(email: string): Observable<any> {
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http.post<any>(
      `${environment.apiUrl}/api/auth/refresh-password/${email}`,
      {},
      { headers: headers, withCredentials: true }
    );
  }

  refreshPasswordSave(token: string, password: string): Observable<any> {
    let params = JSON.stringify({ password: password });
    let headers = new HttpHeaders().set("Content-Type", "application/json");
    return this.http.post<any>(
      `${environment.apiUrl}/token/refresh-password/${token}`,
      params,
      { headers: headers, withCredentials: true }
    );
  }

}
