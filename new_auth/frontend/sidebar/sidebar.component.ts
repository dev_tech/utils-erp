import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '@app/_services';

declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
    scope?: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard',  icon: 'ni-tv-2 text-primary', class: '', scope: 'dashboard-entries'},
  { path: '/board', title: 'Tablero',  icon:'ni-atom text-blue', class: '', scope: 'board-entries'},
  { path: '/entries/create', title: 'Ingreso',  icon:'ni-ruler-pencil text-green', class: '', scope: 'reception-entry' },
  { path: '/entries/delivery', title: 'Entrega',  icon:'ni-box-2 text-primary', class: '', scope : 'deliver-entry' },
  { path: '/entries', title: 'Ordenes',  icon:'ni-archive-2 text-yellow', class: '' , scope: 'list-entries'},
  { path: '/clients', title: 'Clientes',  icon:'ni-satisfied text-orange', class: '' , scope:'list-clients'},
    { path: '/user-profile', title: 'Mi perfil',  icon:'ni-single-02 text-yellow', class: ''},
];

export const ROUTESSETTINGS: RouteInfo[] = [
  { path: '/typeOrders', title: 'Tipos de orden',  icon:'ni-circle-08 text-green', class: '', scope: 'list-typeOrders' },
  { path: '/settings', title: 'Ajustes',  icon:'ni-settings text-gray', class: '', scope: 'update-org' },
];

export const ROUTESAUTH: RouteInfo[] = [
  { path: '/roles', title: 'Roles',  icon:'ni-circle-08 text-red', class: '', scope: 'list-roles' },
  { path: '/backendScopes', title: 'Backend Scopes',  icon:'ni-key-25 text-red', class: '',  scope: 'list-backendScopes'},
  { path: '/scopes', title: 'Scopes',  icon:'ni-key-25 text-red', class: '', scope: 'list-scopes'},
  { path: '/users', title: 'Usuarios',  icon:'ni-single-02 text-red', class: '', scope:'list-users' },
];



@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  public menuItems: any[];
  public menuItemsAuth: any[];
  public menuItemsSettings: any[];
  public isCollapsed = true;

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    let user = this.authenticationService.userValue;
    console.log(user);
    this.menuItems = ROUTES.filter(menuItem => menuItem&&(!menuItem.scope||(user.frontendScopes.indexOf(menuItem.scope)>=0)));
    this.menuItemsAuth = ROUTESAUTH.filter(menuItem => menuItem&&(!menuItem.scope||(user.frontendScopes.indexOf(menuItem.scope)>=0)));
    this.menuItemsSettings = ROUTESSETTINGS.filter(menuItem => menuItem&&(!menuItem.scope||(user.frontendScopes.indexOf(menuItem.scope)>=0)));
    this.router.events.subscribe((event) => {
      this.isCollapsed = true;
   });
  }
}
