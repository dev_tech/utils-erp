import { defaultAPIController } from "./defaultAPI.controller";

import createError from "http-errors";
import { JWT } from "../jwt/token";
import globalMongoConnection from "../helpers/db";
import { IBackendScope } from "../models/backendScope.model";
import { IFrontendScope } from "../models/frontendScope.model";
import { IUser } from "../models/user.model";
import { IRol } from "../models/rol.model";

import paypalService from "../paypal/suscriptions";
import { IOrg } from "../models/org.model";
import googleConnections from '../helpers/googleConnections';

const BackendScope = require("../models/backendScope.model");
const FrontendScope = require("../models/frontendScope.model");
const RefreshToken = require("../models/refresh-token.model");
const User = require("../models/user.model");
export class AuthController extends defaultAPIController {
  constructor() {
    super();
    this.elementModel = User;
    this.singleID = "user";
    this.pluralID = "users";
    this.modelName = "User";
    this.forceDB = "metabase";
  }

  

  /**
   * Crear un usuario(no admin) desde el panel de control
   * @param req 
   * @param res 
   * @param next 
   */
  create = (req, res, next) => {
    const model = globalMongoConnection.connection.useDb("metabase").model(this.modelName);
    const modelScope = globalMongoConnection.connection.useDb("metabase").model("BackendScope");
    const modelOrg = globalMongoConnection.connection.useDb("metabase").model("Org");
    let userToSave: IUser;
    let savedUser = false;
    modelScope
    .find({ default: true })
      .then((scopes) => {
        const newUser = req.body;
        newUser.org = req.user.org;
        newUser.scopes = scopes.map((e) => e._id);
        return model.add(newUser);
      })
      .then((user) => {
        userToSave = user;
        savedUser = true;
        return modelOrg.findById(req.user.org);
      })
      .then((org)=>{
        // Aumenta la cantidad de usuarios de la organizacion, esto sirve para comparar con la suscripcion de paypal en login
        org.quantity = org.quantity+1;
        return org.save();
      })
      .then((org: IOrg)=>{
        // Enviar la peticion para modificar el numero de productos
        return paypalService.revise(org.idSuscription,org.quantity);
      })
      .then((respPaypal: any)=>{
        userToSave.enviar_email_bienvenida();
        console.log(respPaypal);
        // Se obtiene en approveLink del cuerpo de la respuesta y se envia a paypal
        const approveLink = respPaypal.links.find((e:any)=>e.rel === 'approve');
        console.log(approveLink);
        res.status(200).send({
          user: userToSave,
          approveLink: approveLink.href
        });
      })
      .catch((err) => {
        next(createError(err.statusCode, err.message));
      });
  };

  

  authenticate = (req, res, next) => {
    if (!req.body.username || !req.body.password) return next(createError(400, "Faltan campos obligatorios"));
    const ipAddress = req.ip;
    const model = globalMongoConnection.connection.useDb("metabase").model(this.modelName);
    const modelOrg = globalMongoConnection.connection.useDb("metabase").model('Org');
    let userG: IUser;
    let orgG: IOrg;
    let subscriptionG: any;
    model
      .findByEmailOrUsernameWithScopes(req.body.username) // Buscar al usuarios
      .then((user: IUser) => {
        // Crear token
        if (!user) throw createError(404, "No se encontro el usuario");
        if (!user.validPassword(req.body.password)) throw createError(401, "Las credenciales enviadas no son correctas");
        if (!user.verificado) throw createError(401, "La cuenta aun no ha sido verficada, por favor revise su correo electronico");
        if (!user.activo) throw createError(401, "La cuenta no se encuentra activa, contacte a su administrador");
        userG = user;
        return modelOrg.findById(user.org);
      })
      .then((org: IOrg)=>{
        if(!org) throw createError(500, 'No se encontro la organizacion');
        orgG = org;
        // Obtiene la info de la suscripcion
        return paypalService.get(org.idSuscription);
      })
      .then(async (subscription:any)=>{
        subscriptionG = subscription;
        if(!(subscription.status==='ACTIVE'||orgG.forceSuscription)) throw createError(402, 'No tiene una suscription de Paypal activa');
        if(parseInt(subscription.quantity) !== orgG.quantity) throw createError(452, 'Tiene confirmaciones de actualizacion de suscripcion pendientes');
        /* Almacenar usuario ... */
        const userToken = this.getUserToGenToken(userG);
        // console.log(userToken);
        const jwtToken = JWT.generateToken(userToken);
        const refreshToken = JWT.generateRefreshToken(userG, ipAddress);
        await refreshToken.save();
        return {
          ...this.basicDetails(userG),
          jwtToken,
          refreshToken: refreshToken.token,
          frontendScopes: userToken.frontendScopes,
        };
      })
      .then(({ refreshToken, ...user }) => {
        // Devolverlo
        this.setTokenCookie(res, refreshToken);
        res.status(200).json(user);
      })
      .catch((err) => {
        if(err.statusCode===402){
          res.status(402).send({
            message: err.message,
            tryActivate: userG.roles[0].name === 'Admin',
            actualStatus: subscriptionG.status,
            idSuscription: orgG.idSuscription
          })
        }
        else if(err.statusCode===452){
          console.log(orgG.quantity);
        console.log(subscriptionG.quantity);
          res.status(452).send({
            message: err.message,
            retry: userG.roles[0].name === 'Admin',
            quantity: orgG.quantity,
            idSuscription: orgG.idSuscription
          })
        }
        else{
          next(createError(err.statusCode, err.message));
        }
      });
  };


  /**
   * Elimina un usuario (normal)
   * @param req 
   * @param res 
   * @param next 
   */
  delete = (req, res, next)=>{
    const element = res.element;
    const modelOrg = globalMongoConnection.connection.useDb("metabase").model('Org');
    let dataG:any;
    element
      .remove()
      .then((data) => {
        dataG = data;
        return modelOrg.findById(req.user.org);
      })
      .then(org=>{
        org.quantity = org.quantity - 1;
        return org.save();
      })
      .then((org: IOrg)=>{
        return paypalService.revise(org.idSuscription,org.quantity);
      })
      .then((respPaypal: any)=>{
        const approveLink = respPaypal.links.find((e:any)=>e.rel === 'approve');
        res.status(200).send({
          approveLink: approveLink.href,
          element: dataG
        });
      })
      .catch((err: any) => {
        next(createError(err.statusCode, err.message));
      });
  }
}