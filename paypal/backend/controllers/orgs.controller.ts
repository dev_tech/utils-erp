import { defaultAPIController } from "./defaultAPI.controller";
import globalMongoConnection from "../helpers/db";
import paypalService from "../paypal/suscriptions";
import createError from "http-errors";
import { IOrg } from "../models/org.model";

const Org = require("../models/org.model");
export class OrgsController extends defaultAPIController {
  constructor() {
    super();
    this.elementModel = Org;
    this.singleID = "org";
    this.pluralID = "orgs";
    this.modelName = "Org";
    this.forceDB = "metabase";
  }

  suspendAccount = (req, res, next) => {
    let org: IOrg = res.element;
    paypalService
      .suspend(org.idSuscription)
      .then((resp) => {
        console.log(resp);
        res.send({ activation: true });
      })
      .catch((err) => {
          console.log(err);
        next(createError(400, "Error al suspender la suscripcion"));
      });
  };

  deleteAccount = (req, res, next) => {
    let org: IOrg = res.element;
    const userModel = globalMongoConnection.connection
      .useDb("metabase")
      .model("User");
    const orgModel = globalMongoConnection.connection
      .useDb("metabase")
      .model("Org");
    const rolModel = globalMongoConnection.connection
      .useDb("metabase")
      .model("Rol");
    // Eliminar los usuarios
    userModel
      .deleteMany({ org: req.user.org })
      .then((resp) => {
        // Eliminar la base de datos
        return globalMongoConnection.connection
          .useDb(req.user.org)
          .dropDatabase();
      })
      .then((resp) => {
        // Eliminar la organizacions
        return orgModel.findByIdAndRemove(req.user.org);
      })
      .then((org) => {
        // Eliminar roles
        return rolModel.deleteMany({ org: req.user.org });
      })
      .then((roles) => {
          // Cancelar la suscripcion
        return paypalService.cancel(org.idSuscription);
      })
      .then((resp) => {
        res.send({ delete: true });
      })
      .catch((err) => {
          console.log(err);
        next(createError(400, "Error al eliminar la sucripcion"));
      });
  };
}
