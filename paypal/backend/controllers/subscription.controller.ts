import createError from "http-errors";
import paypalService from "../paypal/suscriptions";
import globalMongoConnection from '../helpers/db';

export class SubscriptionController {

  // constructor() {
  // }

  // isActive = (req, res, next) => {
  //   if(!req.params.id) return next(createError(400, 'Se necesita el id de la suscripcion'));
  //   this.paypalService
  //   .get(req.params.id)
  //   .then((suscription) => {
  //     console.log(suscription);
  //     res.status(200).send(productResp);
  //   })
  //   .catch((err) => {
  //     console.log(err);
  //     next(createError(err.statusCode, err.message));
  //   });
  // }

  activate = (req, res, next)=>{
    if(!req.params.id) return next(createError(400, 'Se necesita el id de la suscripcion'));
    paypalService.activate(req.params.id).then(resp=>{
      console.log(resp);
      res.send({activation: true});
    })
    .catch(err=>{
      next(createError(400, 'La suscripcion no puede ser activada'));
    })
  }

  getStatus = (req, res, next)=>{
    if(!req.params.id) return next(createError(400, 'Se necesita el id de la suscripcion'));
    paypalService.get(req.params.id).then(resp=>{
      console.log(resp);
      res.send({status: resp.status});
    })
    .catch(err=>{
      next(createError(err.statusCode, err.message));
    })
  }

  get = (req, res, next)=>{
    if(!req.params.id) return next(createError(400, 'Se necesita el id de la suscripcion'));
    paypalService.get(req.params.id).then(resp=>{
      console.log(resp);
      res.send(resp);
    })
    .catch(err=>{
      next(createError(err.statusCode, err.message));
    })
  }

  updateSubscription  = (req, res, next)=>{
    console.log(req.body);
    if(!req.body.newIdSuscription||!req.body.oldIdSuscription) return next(createError(400, 'Faltan campos obligatorios'));
    const model =  globalMongoConnection.connection.useDb("metabase").model('Org');
    model.findOne({idSuscription: req.body.oldIdSuscription}).then(org=>{
      if(!org) throw createError(501, 'No se encontro la organizacion')
      org.idSuscription = req.body.newIdSuscription;
      return org.save();
    })
    .then(org=>{
      res.send({update: true});
    })
    .catch(err=>{
      next(createError(err.statuscode, err.message));
    })
  }

  revise  = (req, res, next)=>{
    if(!req.params.id) return next(createError(400, 'Se necesita el id de la suscripcion'));
    paypalService.revise(req.params.id, req.body.quantity).then(resp=>{
      console.log(resp);
      res.send(resp);
    })
    .catch(err=>{
      next(createError(400, 'La suscripcion no puede ser activada'));
    })
  }
}
