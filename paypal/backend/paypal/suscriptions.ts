import req from "../helpers/httpPromise";
var querystring = require("querystring");

let pathBase = "/v1/billing/subscriptions";
import createError from "http-errors";

export class PaypalService {
  _token: string;
  _tokenExpires: Date;

  constructor() {}

  //** Obtiene el token, si aun esta vigente el token(tokenExpires) y existe en memoria lo devuelve si no lo manda a traer a paypal */
  getToken(): Promise<string> {
    return new Promise((resolve, reject) => {
      if (this._tokenExpires > new Date()) {
        return resolve(this._token);
      } else {
        this.getTokenExtern()
          .then((resp) => {
            if (!resp.body)
              throw createError(400, "No viene el body en la peticion");
            let tokeninfo = JSON.parse(resp.body);
            this._token = tokeninfo.access_token;
            let now = new Date();
            let dateexpires = now.setSeconds(tokeninfo.expires_in - 100);
            this._tokenExpires = new Date(dateexpires);
            return resolve(this._token);
          })
          .catch((err) => {
            reject(err);
          });
      }
    });
  }

  /** Solicitud a paypal para traer el token */
  getTokenExtern(): Promise<any> {
    var path = "/v1/oauth2/token";
    var postData = querystring.stringify({
      grant_type: "client_credentials",
    });
    var options = {
      host: process.env.urlPaypal,
      path: path,
      method: "POST",
      encoding: null,
      auth: `${process.env.clientIdPaypal}:${process.env.secretPaypal}`,
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
        "Content-Length": postData.length,
      },
    };
    return req(options, postData);
  }

  /**
   * Obtiene la informacion de una suscripcion
   * @param id Id de la suscripcion
   */
  get(id: string): Promise<any> {
    var path = `${pathBase}/${id}`;
    console.log(path);
    return new Promise((resolve, reject) => {
      this.getToken()
        .then((token) => {
          var options = {
            host: process.env.urlPaypal,
            path: path,
            method: "GET",
            encoding: null,
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
          };
          req(options, "")
            .then((resp: any) => {
              var bodyResp = JSON.parse(resp.body);
              resolve(bodyResp);
            })
            .catch((err) => {
              reject(err);
            });
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  /**
   * Devuelve true si la suscripcion esta activa si no false
   * @param id Id de la suscripcion
   */
  isActive(id: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.get(id)
        .then((suscription) => {
          resolve(suscription.status === "ACTIVE");
        })
        .catch((err) => {
          reject(err);
        });
    });
  }

  /**
   * Activa una suscripcion
   * @param id Id de la suscripcion
   */
  activate(id: string): Promise<any> {
    var path = `${pathBase}/${id}/activate`;
    return new Promise((resolve, reject) => {
      this.getToken()
        .then((token) => {
          var options = {
            host: process.env.urlPaypal,
            path: path,
            method: "POST",
            encoding: null,
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
          };
          return req(options, "");
        })
        .then((resp: any) => {
          console.log(resp);
          resolve({activate: true});
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  }

  /**
   * Suspende una suscripcion (la pone en pausa)
   * @param id Id de la suscripcion
   */
  suspend(id: string): Promise<any> {
    var path = `${pathBase}/${id}/suspend`;
    return new Promise((resolve, reject) => {
      this.getToken()
        .then((token) => {
          var options = {
            host: process.env.urlPaypal,
            path: path,
            method: "POST",
            encoding: null,
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
          };
          return req(options, "");
        })
        .then((resp: any) => {
          console.log(resp);
          resolve({suspend: true});
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  }

  /**
   * Cancela una suscripcion
   * @param id Id de la suscripcion
   */
  cancel(id: string): Promise<any> {
    var path = `${pathBase}/${id}/cancel`;
    return new Promise((resolve, reject) => {
      this.getToken()
        .then((token) => {
          var options = {
            host: process.env.urlPaypal,
            path: path,
            method: "POST",
            encoding: null,
            headers: {
              "Content-Type": "application/json",
              Authorization: `Bearer ${token}`,
            },
          };
          return req(options, "");
        })
        .then((resp: any) => {
          console.log(resp);
          resolve({cancel: true});
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  }


  /**
   * Modifica el numero de productos de una suscripcion
   * @param id Id de la suscripcion
   * @param quantity Cantidad que se pondra en la sucripcion
   */
  revise(id: string, quantity: number): Promise<any> {
    let path = `${pathBase}/${id}/revise`;
    return new Promise((resolve, reject) => {
      this.getToken()
        .then((token) => {
          let postData = JSON.stringify({
            quantity: quantity,
          });
          let options = {
            host: process.env.urlPaypal,
            path: path,
            method: "POST",
            encoding: null,
            headers: {
              "Content-Type": "application/json",
              "Content-Length": postData.length,
              Authorization: `Bearer ${token}`,
            },
          };
          return req(options, postData);
        })
        .then((resp: any) => {
          console.log(resp);
          let bodyResp = JSON.parse(resp.body);
          resolve(bodyResp);
        })
        .catch((err) => {
          console.log(err);
          reject(err);
        });
    });
  }
}

export enum HttpMethods {
  POST = "POST",
  GET = "GET",
  PATCH = "PATCH",
  DELETE = "DELETE",
}

export default new PaypalService();