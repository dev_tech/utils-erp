import { OrgsController } from "../../controllers/orgs.controller";
import { DefaultAPIRouter } from "./defaultAPI.router";
import { JWT } from '../../jwt/token';
export class OrgsRouter extends DefaultAPIRouter{
    controller: OrgsController;
    constructor(){
        super(new OrgsController());
        this.router.post("/checkDuplicates", this.controller.checkDuplicates);
        this.router.post('/suspendAccount/:id',JWT.validateToken, this.controller.getElementMiddleware, this.controller.suspendAccount); // Editar uno
        this.router.post('/deleteAccount/:id',JWT.validateToken, this.controller.getElementMiddleware, this.controller.deleteAccount); // Editar uno
    }

}