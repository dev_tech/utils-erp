import express, { Router } from "express";
import { SubscriptionController } from "../../controllers/subscription.controller";

export class SubscriptionRouter {
  router: Router;
  controller: SubscriptionController;
  constructor() {
    this.controller = new SubscriptionController();
    this.router = express.Router();
    this.router.post("/activate/:id", this.controller.activate);
    this.router.post("/revise/:id", this.controller.revise);
    this.router.get("/getStatus/:id", this.controller.getStatus);
    this.router.get("/:id", this.controller.get);
    this.router.patch("/updateSubscription", this.controller.updateSubscription);
  }
}

