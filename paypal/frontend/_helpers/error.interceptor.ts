import { Injectable } from "@angular/core";
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
} from "@angular/common/http";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";

import { AuthenticationService } from "@app/_services";
import { Router } from "@angular/router";
import { PaypalService } from "@app/_services/paypal.service";

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {
  constructor(
    private authenticationService: AuthenticationService,
    private paypalService: PaypalService,
    private router: Router
  ) {}

  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      catchError((err) => {
        let error = (err && err.error && err.error.message) || err.statusText;
        if (
          [401, 403].includes(err.status) &&
          this.authenticationService.userValue
        ) {
          // auto logout if 401 or 403 response returned from api
          this.authenticationService.logout();
        }
        if (err.status == 402) {
          if (err.error.tryActivate) {
            this.router.navigate(
              ["reactivateAccount", err.error.idSuscription],
              { queryParams: { actualStatus: err.error.actualStatus } }
            );
          } else {
            error =
              "El pago de la cuenta no esta activo, por favor contacte a su administrador";
          }
        }
        else if (err.status == 452) {
          if (err.error.retry) {
            this.paypalService.revise(err.error.idSuscription,err.error.quantity).subscribe(response=>{
              console.log(response);
              let approveLink = response.links.find(e=>e.rel === 'approve');
              if(approveLink){
                window.location.href = approveLink.href;
              }
            }, error=>{
              console.log(error);
            })
          } else {
            error =
              "Hay un problema con la activacion de una cuenta, por favor contacte a su administrador";
          }
        }
        console.error(err);
        return throwError(error);
      })
    );
  }
}
