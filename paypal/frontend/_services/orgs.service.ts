import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { Observable } from 'rxjs';
import { DefaultService } from './default.service';

@Injectable({ providedIn: 'root' })
export class OrgsService extends DefaultService {

    constructor( http: HttpClient
    ) {
       super(http)
       this.baseUrl = '/api/orgs'
       this.singId = 'org';
       this.pluralId = 'orgs';
    }

    suspendAccount(idOrg:string): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post<any>(`${environment.apiUrl}${this.baseUrl}/suspendAccount/${idOrg}`, { headers: headers, withCredentials: true });
    }

    deleteAccount(idOrg:string): Observable<any> {
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post<any>(`${environment.apiUrl}${this.baseUrl}/deleteAccount/${idOrg}`, { headers: headers, withCredentials: true });
    }

    checkDuplicate(id: string){
        let params = JSON.stringify({
            id: id
        });
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post<any>(`${environment.apiUrl}${this.baseUrl}/checkDuplicates`, params, { headers: headers, withCredentials: true });
    }
}