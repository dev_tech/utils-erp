import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

import { environment } from '@environments/environment';
import { Observable } from 'rxjs';

@Injectable({ providedIn: 'root' })
export abstract class PaypalService {

    constructor(
        protected http: HttpClient
    ) {
       this.baseUrl="/subscription"
    }

    protected baseUrl : String;
    singId : String;
    pluralId: String;

    activate(id:any):Observable<any> {
		let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post<any>(`${environment.apiUrl}${this.baseUrl}/activate/${id}`, { headers: headers, withCredentials: true });
    }
    revise(id:any, quantity: number):Observable<any> {
        let params = JSON.stringify({quantity: quantity});
		let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post<any>(`${environment.apiUrl}${this.baseUrl}/revise/${id}`, params, { headers: headers, withCredentials: true });
    }
    updateSubscription(obj: any):Observable<any> {
        let params = JSON.stringify(obj);
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.patch<any>(`${environment.apiUrl}${this.baseUrl}/updateSubscription`, params, { headers: headers, withCredentials: true });
    }

}