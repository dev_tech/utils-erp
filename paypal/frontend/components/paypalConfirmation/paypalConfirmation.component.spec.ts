import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PaypalConfirmationComponent } from './paypalConfirmation.component';

describe('PaypalConfirmationComponent', () => {
  let component: PaypalConfirmationComponent;
  let fixture: ComponentFixture<PaypalConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PaypalConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PaypalConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
