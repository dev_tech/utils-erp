import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { User } from "@app/_models/user";
import { AuthenticationService } from "@app/_services";
import { LoggingService } from "@app/_services/logging.service";
import { UsersService } from "@app/_services/users.service";
import { ToastrService } from "ngx-toastr";
import { of } from "rxjs";

@Component({
  selector: "app-paypalConfirmation",
  templateUrl: "./paypalConfirmation.component.html",
  styleUrls: ["./paypalConfirmation.component.scss"],
})
export class PaypalConfirmationComponent implements OnInit {
  public user: User;
  constructor(
    private toastr: ToastrService,
    private authenticationService: AuthenticationService,
    private userService: UsersService,
    private _route: ActivatedRoute,
    private _router: Router,
    private logService: LoggingService
  ) {}
  mine = true;
  userId: string;
  loadingPage = true;
  returnUrl: string;
  ngOnInit() {
    this._route.params.subscribe((params) => {
      if (params.id) {
        this.mine = false;
        this.userId = params.id;
      }
      this.getElement();
    });
    this.returnUrl = this._route.snapshot.queryParams["returnUrl"] || "/users";
  }

  private getElement() {
    // Invocar al servicio
    const observable = this.mine
      ? this.userService.getMine()
      : this.userService.getById(this.userId);
    observable.subscribe(
      (element) => {
        if (element) {
          this.user = new User(element);
          console.log(element);
          this.loadingPage = false;
        } else {
          this.toastr.error(
            "No se obtuvo la respuesta esperada del servidor",
            "Error"
          );
        }
      },
      (error) => {  
        this.logService.error(error);
        this.toastr.error(<any>error, "Error");
      }
    );
  }

  refreshPassword() {
    this.authenticationService
      .refreshPasswordGenerate(this.user.email)
      .subscribe(
        (element) => {
          if (element) {
            this.toastr.success(
              "Se envio el link para el cambio de contraseña a su correo electrónico",
              "Exito"
            );
          } else {
            this.toastr.error(
              "No se obtuvo la respuesta esperada del servidor",
              "Error"
            );
          }
        },
        (error) => {
          this.logService.error(error);
          this.toastr.error(<any>error, "Error");
        }
      );
  }

  return() {
    this._router.navigate([this.returnUrl]);
  }

  goToEdit() {
    if (this.mine) {
      this._router.navigate(["users", "updateMine"]);
    } else {
      this._router.navigate(["users", "update", this.user.id]);
    }
  }
}
