import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { MustMatch } from "@app/_helpers/must-match.validator";
import { Org } from "@app/_models/org";
import { AuthenticationService } from "@app/_services";
import { OrgsService } from "@app/_services/orgs.service";
import { PaypalService } from "@app/_services/paypal.service";
import { ToastrService } from "ngx-toastr";
import { first } from "rxjs/operators";

// import custom validator to validate that password and confirm password fields match
declare let paypal: any;
@Component({
  selector: "app-reactivateAccount",
  templateUrl: "./reactivateAccount.component.html",
  styleUrls: ["./reactivateAccount.component.scss"],
})
export class ReactivateAccountComponent implements OnInit {
  @ViewChild("paypal_button_container") paypalElement: ElementRef;
  registerForm: FormGroup;
  loading = false;
  submitted = false;
  error = "";
  actualStatus: string;
  subscriptionId: string;
  //paypal
  private addScript;
  private paymentSuccess = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private toastr: ToastrService,
    private paypalService: PaypalService,
  ) {
    // redirect to home if already logged in
    if (this.authenticationService.userValue) {
      this.router.navigate(["/"]);
    }
  }

  org: Org;

  ngOnInit() {
    this.actualStatus = this.route.snapshot.queryParams["actualStatus"];
    this.route.params.subscribe((params) => {
      this.subscriptionId = params.id;
    });
    //paypal
    this.addPayPalScript().then((c)=>{
      const self = this;
      paypal.Buttons({
        style: {
            shape: 'rect',
            color: 'blue',
            layout: 'vertical',
            label: 'subscribe'
        },
        createSubscription: function(data, actions) {
          return actions.subscription.create({
            'plan_id': 'P-3P5629036V471720PL6XRK3Q' // Creates the subscription
          });
        },
        onApprove: function(data, actions) {
          console.log(data);
          //alert('You have successfully created subscription ' + data.subscriptionID); // Optional message given to subscriber
          self.onSubmit(data.subscriptionID);
        },
        onCancel: function (data) {
          // Show a cancel page, or return to cart
          console.log(data);

        },
        onError: function (err) {
          // Show an error page here, when an error occurs
          console.log(err);
          this.dat.createAlert("Error",err.message,2,false);
        }
      }).render(this.paypalElement.nativeElement); // Renders the PayPal button
    });
  }

  tryToReactivate() {
    // stop here if form is invalid
    this.paypalService.activate(this.subscriptionId).subscribe(resp=>{
      if(resp){
        this.router.navigate(['login'], {queryParams: {message: 'Se reactivo tu cuenta, puede ingresar de nuevo'}});
      }
    }, err=>{
      this.error
    });
  }

  onSubmit(idSuscription: string) {
    let params ={
      newIdSuscription: idSuscription,
      oldIdSuscription: this.subscriptionId,
    } 
    this.paypalService
      .updateSubscription(params)
      .subscribe({
        next: () => {
          this.router.navigate(["login"], {
            queryParams: {
              message:
                "Se ha vinculado la subscripcion de nuevo con exito.",
            },
          });
        },
        error: (error) => {
          this.error = error;
        },
      });
  }
  
  
  
  //paypal

  addPayPalScript() {
    this.addScript = true;
    return new Promise((resolve, reject) => {
      let scriptTagElement = document.createElement("script");
      scriptTagElement.src =
        "https://www.paypal.com/sdk/js?client-id=AVHNCbWPVUFUcZaT0lVRnCNaYVCm04nlisws9GzsvrYnu5lD4MibO172oHXZn2vF1Su2AFlf4siA48Kc&vault=true&intent=subscription";
      scriptTagElement.onload = resolve;
      document.body.appendChild(scriptTagElement);
    });
  }
}
