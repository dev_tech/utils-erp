import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScopesCreateComponent } from './create.component';

describe('ServicesSeeComponenet', () => {
  let component: ScopesCreateComponent;
  let fixture: ComponentFixture<ScopesCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScopesCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScopesCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
