import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { Router } from "@angular/router";
import { RolesService } from "@app/_services/roles.service";

@Component({
  selector: "app-roles-create",
  templateUrl: "./create.component.html",
  styleUrls: ["./create.component.scss"],
})
export class UsersCreateComponent implements OnInit {
  constructor(
    private _router: Router,
  ) {
  }

  ngOnInit() {
  }

  goToList(){
    this._router.navigate(['users']);
  }
}
