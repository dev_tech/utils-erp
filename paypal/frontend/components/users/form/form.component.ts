import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ToastrService } from "ngx-toastr";
import { Rol } from "@app/_models/rol";
import { ConfirmationDialogService } from "@app/components/confirmation_dialog/confirmation-dialog.service";
import { Observable } from "rxjs";
import { RolesService } from "@app/_services/roles.service";
import { Router } from "@angular/router";
import { UsersService } from "@app/_services/users.service";
import { User } from "@app/_models";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { MustMatch } from "@app/_helpers/must-match.validator";
import { LoggingService } from "@app/_services/logging.service";

@Component({
  selector: "app-users-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"],
})
export class UsersFormComponent implements OnInit {
  constructor(
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private _rolesService: RolesService,
    private _dataService: UsersService,
    private _router: Router,
    private logService: LoggingService,
    private confirmationDialogService: ConfirmationDialogService
  ) {}
  loading = false;
  submitted = false;
  error = "";
  user: User;
  @Input() type: string;
  @Input() idUser: string;
  roles$: Observable<Rol[]>; //Para el select de dependencias
  userForm: FormGroup;
  loadingPage = false;

  ngOnInit() {
    this.user = new User({});
    this.roles$ = this._rolesService.getAll();
    if (this.type == "update" || this.type == "updateMine") this.getUser();
    this.userForm = this.formBuilder.group(
      {
        name: ["", Validators.required],
        lastName: ["", Validators.required],
        username: ["", Validators.required],
        email: ["", Validators.email],
        password: ["", Validators.minLength(6)],
        confirmPassword: ["", Validators.required],
        tel: [""],
        roles: [[]],
      },
      {
        validator: MustMatch("password", "confirmPassword"),
      }
    );
  }

  /**
   * Obtiene el rol a actualizar
   */
  getUser() {
    this.loadingPage = true;
    let observable =
      this.type == "updateMine"
        ? this._dataService.getMine()
        : this._dataService.getById(this.idUser);
    observable.subscribe(
      (element) => {
        if (element) {
          // Agregarle la propiedad de assign
          this.user = element;
          this.userForm.setValue({
            name: this.user.name,
            lastName: this.user.lastName,
            username: this.user.username,
            password: "password",
            confirmPassword: "password",
            email: this.user.email,
            tel: this.user.tel?.length > 0 ? this.user.tel[0] : "",
            roles: this.user.roles,
          });
        } else {
          this.toastr.error(
            "No se obtuvo la respuesta esperada del servidor",
            "Error"
          );
        }
        this.loadingPage = false;
      },
      (error) => {
        this.logService.error(error);
        this.toastr.error(<any>error, "Error");
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.userForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.userForm.invalid) {
      return;
    }
    this.loading = true;
    let usertosave = this.userForm.value;
    usertosave.tel = [usertosave.tel];
    if (this.type == "update" || this.type == "updateMine") {
      delete usertosave.password;
      delete usertosave.confirmPassword;
      delete usertosave.username;
    }
    if (this.type == "updateMine") {
      delete usertosave.roles;
    }
    switch (this.type) {
      case "create":
        this.confirmationDialogService
          .confirm(
            "Agregar un usuario",
            `Al agregar un usuario se le cobraran $10.00 adicionales en la cuota de su suscripcion. Si esta de acuerdo puede continuar con el proceso, si no presione cancelar.`
          )
          .then((confirmed) => {
            if (confirmed) {
              this.executeSave(this._dataService.create(usertosave));
            } else {
              this.loading = false;
              return;
            }
          });
        break;
      case "update":
        this.executeSave(this._dataService.update(this.user.id, usertosave));
        break;
      case "updateMine":
        this.executeSave(this._dataService.updateMine(usertosave));
        break;
    }
  }

  executeSave(observable: Observable<any>) {
    observable.subscribe(
      (response) => {
        if (response) {
          this.toastr.success("Operacion realizada con exito", "Exito");
          console.log(response);
          if(response.approveLink){
            window.location.href = response.approveLink;
          }
          else{
            if (this.type == "updateMine") {
              this._router.navigate(["user-profile"]);
            } else {
              this._router.navigate(["users"]);
            }
          }
        } else {
          this.toastr.error(
            "No se obtuvo la respuesta esperada del servidor",
            "Error"
          );
          this.loading = false;
        }
      },
      (error) => {
        this.logService.error(error);
        this.toastr.error(<any>error, "Error");
        this.loading = false;
      }
    );
  }
}
