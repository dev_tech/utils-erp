import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-users-update",
  templateUrl: "./update.component.html",
  styleUrls: ["./update.component.scss"],
})
export class UsersUpdateComponent implements OnInit {
  constructor(
    private _router: Router,
    private _route: ActivatedRoute
  ) {
  }
  idUser: string;

  ngOnInit() {
    this._route.params.subscribe((params) => {
      this.idUser = params.id;
    });
  }

  goToList(){
    this._router.navigate(['users']);
  }
}
