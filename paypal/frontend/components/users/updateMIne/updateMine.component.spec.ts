import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersUpdateMineComponent } from './updateMine.component';

describe('ServicesSeeComponenet', () => {
  let component: UsersUpdateMineComponent;
  let fixture: ComponentFixture<UsersUpdateMineComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersUpdateMineComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersUpdateMineComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
