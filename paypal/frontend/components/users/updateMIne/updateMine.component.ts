import { Component, Input, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-users-updateMine",
  templateUrl: "./updateMine.component.html",
  styleUrls: ["./updateMine.component.scss"],
})
export class UsersUpdateMineComponent implements OnInit {
  constructor(
    private _router: Router,
    private _route: ActivatedRoute
  ) {
  }
  idUser: string;

  ngOnInit() {
  }

  return(){
    this._router.navigate(['user-profile']);
  }
}
