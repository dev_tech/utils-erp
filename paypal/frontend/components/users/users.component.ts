import { Component, OnInit, ViewChild } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { ConfirmationDialogService } from "@app/components/confirmation_dialog/confirmation-dialog.service";
import { DynamicTableComponent } from "@app/components/dynamic-table/dynamic-table.component";
import {
  TableAction,
  TableSettings,
} from "@app/components/dynamic-table/settings.model";
import { User } from "@app/_models/user";
import { AuthenticationService } from "@app/_services";
import { LoggingService } from "@app/_services/logging.service";
import { UsersService } from "@app/_services/users.service";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";
// import { RolesSeeComponent } from "./see/see.component";

@Component({
  selector: "app-users",
  templateUrl: "./users.component.html",
  styleUrls: ["./users.component.scss"],
  providers: [UsersService],
})
export class UsersComponent implements OnInit {
  @ViewChild(DynamicTableComponent, { static: true })
  table: DynamicTableComponent;
  settings_table: TableSettings;
  constructor(
    private _dataService: UsersService,
    private _router: Router,
    private _route: ActivatedRoute,
    private modalService: NgbModal,
    private toastr: ToastrService,
    private logService: LoggingService,
    private authenticationService: AuthenticationService,
    private confirmationDialogService: ConfirmationDialogService
  ) {
    this.settings_table = {
      title: "Users",
      baseUrl: "users",
      sizePage: 10,
      showCreate: true,
      defaultActions: {
        see: false,
        delete: false,
        update: true,
      },
      scopeCreate: 'create-user',
      scopeDelete: 'delete-user',
      scopeUpdate: 'update-user',
      scopeSee: 'see-user',
      columns: [
        {
          title: "Nombre",
          id: "name",
        },
        {
          title: "Apellido",
          id: "lastName",
        },
        {
          title: "Usuario",
          id: "username",
        },
        {
          title: "Correo",
          id: "email",
        },
        {
          title: "Estado",
          id: "status",
          transform: (row: User) => {
            if (!row.verificado)
              return '<span class="badge badge-warning">Por Verificar</span>';
            else if (!row.activo)
              return '<span class="badge badge-danger">Desactivado</span>';
            else row.activo;
            return '<span class="badge badge-success">Activo</span>';
          },
        },
      ],
      actions: [
        {
          title: "Forzar verificacion",
          scope: 'update-user',
          do: (row, table) => {
            this._dataService.forceVerification(row.id).subscribe(
              (element) => {
                if(element){
                  this.toastr.success('Se forzo la verificacion de la cuenta con exito', "Exito");
                  table.getData();
                }
              },
              (error) => {
                this.logService.error(error);
                this.toastr.error(<any>error, "Error");
              }
            );
          },
        },
        {
          title: "Activar/Desactivar",
          scope: 'update-user',
          do: (row, table) => {
            this._dataService.toggleActivation(row.id).subscribe(
              (element) => {
                if(element){
                  this.toastr.success('Se modifico la cuenta con exito', "Exito");
                  table.getData();
                }
              },
              (error) => {
                this.logService.error(error);
                this.toastr.error(<any>error, "Error");
              }
            );
          },
        },
        {
          icon: "",
          title: "Eliminar",
          do: (row, table: DynamicTableComponent) => {
            this.confirmationDialogService
              .confirm(
                "Eliminar elemento",
                `Esta seguro que desea eliminar el elemento ${row.name}? Al eliminar este usuario se reduciran $10.00 de su suscripcion mensual`
              )
              .then((confirmed) => {
                if (confirmed) {
                  this._dataService.delete(row.id).subscribe(
                    (response) => {
                      if (response) {
                        this.toastr.success("Se elimino correctamente", "Exito");
                        this.table.getData();
                        if(response.approveLink){
                          window.location.href = response.approveLink;
                        }
                      } else {
                        this.toastr.error(
                          "No se obtuvo la respuesta esperada por el servidor",
                          "Error"
                        );
                      }
                    },
                    (error) => {
                      this.toastr.error(<any>error, "Error");
                    }
                  );
                }
              });
          },
        }
      ],
      onPickRow: {
        do: (row) => {
          if (this.authenticationService.userValue.can('see-user')) this._router.navigate(["user-profile", row.id]);
        },
      },
    };
  }

  ngOnInit() {
    let user = this.authenticationService.userValue;
    this.table.setService(this._dataService, {username: {$not: { $eq: user.username }}});
  }
}
